# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User

from django.utils import timezone

# Create your models here.

class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_worker = models.BooleanField(default=True)
    register_code = models.CharField(max_length=21, default="")
    is_activated = models.BooleanField(default=False)


class Task(models.Model):
    title = models.CharField(max_length=200)
    desc = models.CharField(max_length=200, default="", blank=True)
    value = models.IntegerField(default=0)
    est_days = models.IntegerField(default=0)
    deadline = models.DateTimeField(default=timezone.now())
    user = models.ForeignKey(User, blank=True, null=True, on_delete = models.SET_NULL)
