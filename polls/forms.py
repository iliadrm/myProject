from django.forms import ModelForm
from django import forms
from .models import Task
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

class LoginForm(forms.Form):
    login_username = forms.CharField(label='Username', max_length=100)
    login_password = forms.CharField(label='Password', max_length=100)

class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'est_days', 'value', 'desc',]

    def clean(self):
        cleaned_data = super(TaskForm, self).clean()
        value = cleaned_data.get('value')
        est_days = cleaned_data.get('est_days')

        if value < 200 or value > 50000:
            raise ValidationError(_('Invalid value, must be between 200 and 50,000'), code='invalid')
        if est_days < 3 and value > 30000:
            raise ValidationError(_('Maximum value of works with less than 3 days is 30,000'), code='invalid')

        return cleaned_data
