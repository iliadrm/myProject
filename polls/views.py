# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404

from django.http import HttpResponse, HttpResponseRedirect

from django.conf import settings

from django.urls import reverse

from django.core.mail import send_mail

import random
import string

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .models import Task, Employee

from django.forms import ModelForm
from .forms import LoginForm, TaskForm
from django_redis import get_redis_connection

def lord_checker(old_function):

    def new_function(*args, **kwds):

        if args[0].user.is_authenticated:
            if Employee.objects.get(user=args[0].user).is_worker:
                context = get_context_create_task(args[0])
                context["not_lord"] = True
                return render(args[0], 'polls/create_task.html', context)
            return old_function(*args, **kwds)
        else:
            context = get_context_create_task(args[0])
            context["not_signed_in"] = True
            return render(args[0], 'polls/create_task.html', context)


    return new_function



def index(request):

    rows_per_page = settings.ROWS_PER_TABLE_PAGE
    poll_url = settings.SERVER_URL + "/polls/"

    tasks = len(Task.objects.all()) > 0

    form = LoginForm()

    context = {
        'tasks' : tasks,
        'form': form,
    }
    if request.user.is_authenticated:
        context['username'] = request.user.username
        try:
            context['is_lord'] = not Employee.objects.get(user=request.user).is_worker
        except Exception as e:
            pass
    return render(request, 'polls/index.html', context)

def load_more_rows(request, task_id_s):

#    return HttpResponse("OK")
    task_id = int(task_id_s)

    n_tasks = len(Task.objects.all())
    if task_id < 0 or task_id >=  n_tasks:
        return ""

    tasks = Task.objects.all()[task_id:min(task_id+7, n_tasks)]

    s = ''
    for task in tasks :
        s += '<tr>'
        s += '<td class="td1">' + task.title + '</td>'
        s += '<td class="td1">' + str(task.value) + u' تومان' + '</td>'
        s += '<td class="td1"><ins>' + str(task.est_days) + '</ins> ' + u'روز' + '</td>'
        s += '<td class="td1"><span class="dutext">' + task.user.username + '</td>'
        s += '<td class="td1" style="text-align:center;"><a style="color:black;text-decoration:none;"href="/polls/task/' + str(task.id)+ '/"><span class="button">توضیحات</span></a></td>'

        s += '</tr>'

    return HttpResponse(s)


    return HttpResponse('''<tr>
        <td class="td1">some task</td>
        <td class="td1">1000 تومان</td>
        <td class="td1"><ins>5</ins> روز</td>
        <td class="td1"><span class="dutext">someone</td>
        <td class="td1" style="text-align:center;"><a style="color:black;text-decoration:none;"href="/polls/task/14/"><span class="button">توضیحات</span></a></td>
    </tr>''')


def task(request, task_id_s):
    if not request.user.is_authenticated:
        context = {'not_signed_in':True,}
        return render(request, 'polls/task.html', context)
        return HttpResponse("<h3>You should sign in to be able to view this page</h3>")

    task = get_object_or_404(Task, id=int(task_id_s))
    context = {'task':task, 'url':'/polls/task_assign/'+task_id_s+'/',}
    if (task.user is not None) or (not Employee.objects.get(user=request.user).is_worker):
        context['disable'] = True

    return render(request, 'polls/task.html', context)

def task_assign(request, task_id_s):
    task = get_object_or_404(Task, id=int(task_id_s))
    task.user = request.user
    task.save()
    return HttpResponseRedirect(reverse('polls:task', kwargs={'task_id_s':task_id_s,}))

def get_context_create_task(request, task_status="None"): #OK, None or error message
    task_form = TaskForm()
    context = {}
    context['username'] = request.user.username
    context['is_lord'] = not Employee.objects.get(user=request.user).is_worker

    context['task_form'] = task_form

    if task_status == "OK":
        context["task_status_ok"] = True
    elif task_status != "None":
        context["task_status_error"] = task_status
    return context

@lord_checker
def create_task(request, task_status="None"):
    context = get_context_create_task(request, task_status)
    return render(request, 'polls/create_task.html', context)

def create_task_submit(request):
    if request.method == "POST":
        task_form = TaskForm(request.POST)
        if task_form.is_valid():
            new_task = task_form.save()
            task_status = "OK"
        else:
            task_status = str(task_form.errors)
    else:
        task_status = "None"

    context = get_context_create_task(request, task_status)
    return render(request, 'polls/create_task.html', context)

    #return HttpResponseRedirect(reverse('polls:create_task', kwargs={'task_status':task_status,}))



def register(request):
    slist = ('success', 'userax', 'passmm')
    sret = slist[0]
    user = request.POST['reg_username']
    pswd = request.POST['reg_password']
    pswd2 = request.POST['reg_password-rep']
    is_worker = request.POST.get('reg_is_lord') is None


    if User.objects.filter(username=user).exists():
        sret = slist[1]
    elif pswd != pswd2:
        sret = slist[2]
    else:
        sret = slist[0]
        email = request.POST['reg_email']
        register_code = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(20))
        new_user = User(username=user, password = pswd, email = email)
        new_user.save()
        new_emp = Employee(user = new_user, register_code = 'redis bitch', is_activated = False, is_worker = is_worker)
        new_emp.save()
        con = get_redis_connection("default")
        con.set(register_code, user)
        link = settings.SERVER_URL + '/polls/register_activation/' + register_code
        send_mail('Hello there', ('Hello Dear ' + user + '\n\tPlease click on the following link: ' + link), 'emailsend82@gmail.com', [email,], fail_silently=False,)



    return HttpResponseRedirect(reverse('polls:register_status', args=(sret,)))

def register_status(request, status):
    dict = {'success' : 'Registered Successfully, please click on the link sent to your mail', 'userax' : 'This user already exists', 'passmm' : 'Passwords do not match'}
    return HttpResponse("<h2>" + dict[status] + "</h2>")

def register_activation(request, register_code):
    con = get_redis_connection("default")
    user = con.get(register_code)
    users = User.objects.filter(username = user)

    emps = Employee.objects.filter(register_code = register_code)
    if users.exists():
        emp = Employee.objects.get(user = users[0])
        username = emp.user.username

        if emp.is_activated :
            return HttpResponse("<h2>" + username + "'s account is already activated</h2>")
        else :
            emp.is_activated = True
            emp.save()
            return HttpResponse("<h2>Dear " +  username + ", your account is successfully activated, now you can login</h2>")
    else :
        return HttpResponse("<h2>Invalid Registration Link</h2>")


def login_user(request):
    if request.method != 'POST':
        return HttpResponse("Invalid request method")

    form = LoginForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data['login_username']
        password = form.cleaned_data['login_password']
        user = authenticate(username = username, password = password)

        users = User.objects.filter(username=username)
        if not users.exists():
            return HttpResponse("<h2>Username does not exist</h2>")
        user2 = users[0]

        #if user is not None
        if user2 is not None and user2.password == password:
            if Employee.objects.get(user=user2).is_activated:
                login(request, user2)
                return HttpResponseRedirect(reverse('polls:index'))
            else:
                return HttpResponseRedirect(reverse('polls:login_inactive'))
        else:
            #return HttpResponse('<h2>$' + user2.password + '$' + password + '$</h2>')
            return HttpResponseRedirect(reverse('polls:login_fail'))
    else:
        return HttpResponse("Please fill the username and password fields properly")

def login_fail(request):
    return HttpResponse("<h2>Username and Password do not match</h2>")

def login_inactive(request):
    return HttpResponse("<h2>Your account is not activated yet!</h2>")

def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('polls:index'))
