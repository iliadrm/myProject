# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from models import Employee, Task
from django.core.mail import send_mail

admin.site.register(Employee)


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    def delete_model(self, request, obj):
        email = obj.user.email
        user = obj.user.username
        send_mail('Hello there', ('Hello Dear ' + user + '\n\tTask that you were assigned to has been deleted.'), 'emailsend82@gmail.com', [email,], fail_silently=False,)
        super(TaskAdmin, self).delete_model(request, obj)
