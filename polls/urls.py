from django.conf.urls import url

from . import views

app_name="polls"
urlpatterns = [
    url(r'^task/(?P<task_id_s>[0-9]+)/$', views.task, name='task'),
    url(r'^register_activation/(?P<register_code>[a-z0-9]+)$', views.register_activation, name='register_activation'),
    url(r'^register_status/(?P<status>[a-z]+)', views.register_status, name='register_status'),
    url(r'^register/', views.register, name='register'),
    url(r'^login_fail', views.login_fail, name='login_fail'),
    url(r'^login_inactive', views.login_inactive, name='login_inactive'),
    url(r'^login_user/', views.login_user, name='login_user'),
    url(r'^logout_user/', views.logout_user, name='logout_user'),
    url(r'^create_task_submit/', views.create_task_submit, name='create_task_submit'),
    url(r'^create_task/', views.create_task, name='create_task'),
    url(r'^task_assign/', views.task_assign, name='task_assign'),
    url(r'^load_more_rows/(?P<task_id_s>[0-9]+)/$', views.load_more_rows, name='load_more_rows'),
#    url(r'^load_more_rows/', views.load_more_rows, name='load_more_rows'),
    url(r'^$', views.index, name='index'),
]
