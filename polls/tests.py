# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from django.urls import reverse

# Create your tests here.
class ViewTests(TestCase):
    def test_respondance_of_views(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        for i in range(100):
            response = self.client.get(reverse('polls:index', args=(str(i),)))
            self.assertEqual(response.status_code, 200)
        
